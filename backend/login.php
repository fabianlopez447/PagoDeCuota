<?php require_once('../Connections/pagodecuotaBD.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['usuario'])) {
  $loginUsername=$_POST['usuario'];
  $password=$_POST['contrasena'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "index.php";
  $MM_redirectLoginFailed = "loginerror.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
  
  $LoginRS__query=sprintf("SELECT usuario, contrasena FROM usuario WHERE usuario=%s AND contrasena=%s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $pagodecuotaBD) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Iniciar Sesion - Panel de Administración</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

</head>
<body>
<div class="container">
<br /><br /><br /><br /><br /><br />
		<div  class="col-md-6 col-md-offset-3">	
        	<div class="panel panel-default">
                 <div class="panel-body">
                 
                   <center> 
                    <h1><span class="label label-info">IUPSM Control de pago de Cuota</span> </h1><br>
                    <h3 ><span class="label label-default"> Inicio de sesion </span> </h3>  <hr /></center>

									<form ACTION="<?php echo $loginFormAction; ?>" METHOD="POST" is="frmlogin" name="frmlogin">
                                   <div class="form-group">       
                                     <input class="form-control"  placeholder="Usuario" type="text"  name="usuario"/>
   									 </div>
                                  	<div class="form-group">
                                    <input class="form-control" placeholder="Contraseña" type="password"   name="contrasena" onfocus="this.value=''" />
                                    </div>
                                    <div class="form-group">
                                        
                                        <button type="submit" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-chevron-right"> </span> Ingresar</button>
                                    </div>
										</form>
                               </div>
                          </div>
                     </div>
</div>
</body>
</html>