<?php require_once('../Connections/pagodecuotaBD.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE estudiante SET nombre=%s, apellido=%s, direccion=%s, telefono=%s, correo=%s WHERE cedula=%s",
                       GetSQLValueString($_POST['nombre'], "text"),
                       GetSQLValueString($_POST['apellido'], "text"),
                       GetSQLValueString($_POST['direccion'], "text"),
                       GetSQLValueString($_POST['telefono'], "text"),
                       GetSQLValueString($_POST['correo'], "text"),
                       GetSQLValueString($_POST['cedula'], "int"));

  mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
  $Result1 = mysql_query($updateSQL, $pagodecuotaBD) or die(mysql_error());

  $updateGoTo = "listaestudiantes.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['cedula'])) {
  $colname_Recordset1 = $_GET['cedula'];
}
mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
$query_Recordset1 = sprintf("SELECT * FROM estudiante WHERE cedula = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $pagodecuotaBD) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ingresar Estudiante</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

</head>

<body>

<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
   <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">  
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" >Control  de pago de cuota</a>
     </div>

          <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li class="active" ><a href="agregar.php"> Ingresar Nuevo </a></li>
            <li ><a href="listaestudiantes.php">Lista de estudiantes</a></li>
          </ul>
      
  </div>
</nav>

<body>


<div class="container">
        <div  class="col-md-6 col-md-offset-3">	
        	<div class="panel panel-default">
                 <div class="panel-body">
                    <h3> Actualizar datos del estudiante </h3><hr />
                    <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
                            <div class="form-group">
                           	  <label>Nombre</label>
                              <input type="text" class="form-control" name="nombre" value="<?php echo htmlentities($row_Recordset1['nombre'], ENT_COMPAT, 'utf-8'); ?>" />
                              </div>  
                            <div class="form-group">  
                             <label>Apellido</label>
 							 <input type="text" class="form-control"  name="apellido" value="<?php echo htmlentities($row_Recordset1['apellido'], ENT_COMPAT, 'utf-8'); ?>"  />
                             </div>
                             <div class="form-group">
                              <label>Dirección</label>
 							  <input type="text" class="form-control"  name="direccion" value="<?php echo htmlentities($row_Recordset1['direccion'], ENT_COMPAT, 'utf-8'); ?>" />
                            </div>
                            <div class="form-group">
                             <label>Telefono</label>
 							 <input type="text" class="form-control"  name="telefono" value="<?php echo htmlentities($row_Recordset1['telefono'], ENT_COMPAT, 'utf-8'); ?>" />
                              </div>
                           <div class="form-group">
                             <label>Correo</label>
 							 <input type="text" class="form-control"  name="correo" value="<?php echo htmlentities($row_Recordset1['correo'], ENT_COMPAT, 'utf-8'); ?>" />
                            </div>
                            <div class="form-group">
                              
                             <button class="btn btn-success pull-right" type="submit" ><span class="glyphicon glyphicon-floppy-saved" > </span>  Guardar cambios </button>
                            </div>
                       <input type="hidden" name="MM_update" value="form1" />
  					   <input type="hidden" name="cedula" value="<?php echo $row_Recordset1['cedula']; ?>" />
                    </form>
                   </div>
              </div>
     </div>
</div>
      

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
