<?php require_once('../Connections/pagodecuotaBD.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "-1";
if (isset($_GET['verestudiante'])) {
  $colname_Recordset1 = $_GET['verestudiante'];
}
mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
$query_Recordset1 = sprintf("SELECT * FROM estudiante WHERE cedula = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $pagodecuotaBD) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$colname_Recordset2 = "-1";
if (isset($_GET['verestudiante'])) {
  $colname_Recordset2 = $_GET['verestudiante'];
}
mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
$query_Recordset2 = sprintf("SELECT * FROM cuota WHERE cedula = %s", GetSQLValueString($colname_Recordset2, "int"));
$Recordset2 = mysql_query($query_Recordset2, $pagodecuotaBD) or die(mysql_error());
$row_Recordset2 = mysql_fetch_assoc($Recordset2);
$totalRows_Recordset2 = mysql_num_rows($Recordset2);

$colname_Recordset3 = "-1";
if (isset($_GET['verestudiante'])) {
  $colname_Recordset3 = $_GET['verestudiante'];
}
mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
$query_Recordset3 = sprintf("SELECT * FROM pago WHERE cedula = %s", GetSQLValueString($colname_Recordset3, "text"));
$Recordset3 = mysql_query($query_Recordset3, $pagodecuotaBD) or die(mysql_error());
$row_Recordset3 = mysql_fetch_assoc($Recordset3);
$totalRows_Recordset3 = mysql_num_rows($Recordset3);

if ($row_Recordset2['cuota1'] == 'Pagado'){
	$escuota1='success';
}else{
	$escuota1='danger';
}

if ($row_Recordset2['cuota2'] == 'Pagado'){
	$escuota2='success';
}else{
	$escuota2='danger';
}

if ($row_Recordset2['cuota3'] == 'Pagado'){
	$escuota3='success';
}else{
	$escuota3='danger';
}

if ($row_Recordset2['cuota4'] == 'Pagado'){
	$escuota4='success';
}else{
	$escuota4='danger';
}

if ($row_Recordset2['cuota5'] == 'Pagado'){
	$escuota5='success';
}else{
	$escuota5='danger';
}

if ($row_Recordset2['cuota6'] == 'Pagado'){
	$escuota6='success';
}else{
	$escuota6='danger';
}


  if (($row_Recordset2['cuota1']==('Pagado'))&&($row_Recordset2['cuota2']==('Pagado'))&&($row_Recordset2['cuota3']==('Pagado'))&&($row_Recordset2['cuota4']==('Pagado'))&&($row_Recordset2['cuota5']==('Pagado'))&&($row_Recordset2['cuota6']==('Pagado'))){
        $ocultar='disabled';
        }else{
        $ocultar=' ';
        }

?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<title>Datos del estudiante</title>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
 	<div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">  
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
      </button>
              <a class="navbar-brand" href="index.php" >Control  de pago de cuota</a>
    </div>
     <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li  ><a href="agregar.php"> Ingresar Nuevo </a></li>
            <li ><a href="listaestudiantes.php">Lista de estudiantes</a></li>
          </ul> 
 	 </div>
  </div>
</nav>

<div class="container">
  <div class="col-md-6">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Datos personales del estudiante</h3>
          </div>
          <div class="panel-body">
            <strong>Cedula: </strong><?php echo $row_Recordset1['cedula']; ?><br>
            <strong>Nombre: </strong><?php echo $row_Recordset1['nombre']; ?><br>
            <strong>Apellido: </strong><?php echo $row_Recordset1['apellido']; ?><br>
            <strong>Direccion: </strong><?php echo $row_Recordset1['direccion']; ?><br>
            <strong>Telefono: </strong><?php echo $row_Recordset1['telefono']; ?><br>
            <strong>Correo: </strong><?php echo $row_Recordset1['correo']; ?><br>
          </div>
        </div>
   </div>
   <div class="col-md-6">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Datos del pago</h3>
          </div>
          <div class="panel-body">
           <strong>Metodo de pago: </strong><?php echo $row_Recordset3['metodo']; ?><br>
           <strong>Numero de comprobante: </strong><?php echo $row_Recordset3['comprobante']; ?><br>
           <strong>Numero de voucher: </strong><?php echo $row_Recordset3['voucher']; ?><br>
           <strong>Banco: </strong><?php echo $row_Recordset3['banco']; ?><br>
           <strong>Fecha de pago: </strong><?php echo $row_Recordset3['fecha']; ?><br>
           <strong>Monto: </strong><?php echo $row_Recordset3['cedula']; ?><br>
          </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h3 class="panel-title">Cuotas <div class="pull-right">Fecha y Hora de ultimo pago: <?php echo $row_Recordset2['fecha']; ?></div></h3>
            
          </div>
          <div class="panel-body">
           <h3> Cuota 1: <span class="label label-<?php echo $escuota1 ?>"> <?php echo $row_Recordset2['cuota1']; ?></span></h3>
           <h3> Cuota 2: <span class="label label-<?php echo $escuota2 ?>"> <?php echo $row_Recordset2['cuota2']; ?></span></h3>
           <h3> Cuota 3: <span class="label label-<?php echo $escuota3 ?>"> <?php echo $row_Recordset2['cuota3']; ?></span></h3>
           <h3> Cuota 4: <span class="label label-<?php echo $escuota4 ?>"> <?php echo $row_Recordset2['cuota4']; ?></span></h3>
           <h3> Cuota 5: <span class="label label-<?php echo $escuota5 ?>"> <?php echo $row_Recordset2['cuota5']; ?></span></h3>
           <h3> Cuota 6: <span class="label label-<?php echo $escuota6 ?>"> <?php echo $row_Recordset2['cuota6']; ?></span></h3>
           
           <a href="editarpago.php?verpago=<?php echo $row_Recordset1['cedula']; ?>"><button class="btn btn-success pull-right" type="button" <?php echo $ocultar ?> ><span class="glyphicon glyphicon-refresh"> </span> Actualizar Pago </button></a>
          </div>
        </div>
     </div>
     
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
<?php
mysql_free_result($Recordset1);

mysql_free_result($Recordset2);

mysql_free_result($Recordset3);
?>
