<?php require_once('../Connections/pagodecuotaBD.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO pago (metodo, comprobante, voucher, banco, fecha, monto,cedula) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['metodo'], "text"),
                       GetSQLValueString($_POST['comprobante'], "text"),
                       GetSQLValueString($_POST['voucher'], "text"),
                       GetSQLValueString($_POST['banco'], "text"),
                       GetSQLValueString($_POST['fecha'], "text"),
                       GetSQLValueString($_POST['monto'], "int"),
					   GetSQLValueString($_POST['cedula'], "text"));

  mysql_select_db($database_pagodecuotaBD, $pagodecuotaBD);
  $Result1 = mysql_query($insertSQL, $pagodecuotaBD) or die(mysql_error());

  $insertGoTo = "pago.php?cedula=".$_POST['cedula'];
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Panel de administración - Pago de Cuota</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    function mostrar(id) {
        if (id == "deposito") {
            $("#deposito").show();
            $("#tarjeta").hide();
        }
    
        if (id == "tarjeta") {
            $("#deposito").hide();
            $("#tarjeta").show();
        }
    }
</script>

</head>

<body>

<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
 	<div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">  
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
      </button>
              <a class="navbar-brand" href="index.php" >Control  de pago de cuota</a>
    </div>
     <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li  ><a href="agregar.php"> Ingresar Nuevo </a></li>
            <li ><a href="listaestudiantes.php">Lista de estudiantes</a></li>
          </ul> 
 	 </div>
  </div>
</nav>

<div class="container-fluid">
	<div class="row">
<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
           	  <div class="panel-body">
                            <h3 class="text-primary">Información de pago</h3><hr>               
                    <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">     
                     <input type="hidden" placeholder="Cedula" name="cedula" value="<?php echo $_GET['cedula'];?>" size="32" />
                    <div class="form-group">
                       <div class="input-group">
                         <div class="input-group-addon"> Metodo de pago</div>      
                        <select id="metodo" class="form-control" name="metodo" onChange="mostrar(this.value);">
                            <option value="Seleccionar">Seleccionar</option>
                            <option value="deposito">Deposito</option>
                            <option value="tarjeta">Tarjeta Debito</option>
                         </select>
                        </div>
                    </div>         
                 <div id="deposito" style="display: none;">  
                     <h3>Metodo de pago: Deposito bancario.</h3>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Numero de voucher" name="voucher"/>
                        </div>
                      <div class="form-group">
                       <div class="input-group">
                         <div class="input-group-addon"> Banco</div>  
                            <select class="form-control"  name="banco" >
                            <option value="BOD">BOD</option>
                            <option value="Provincial">Provincial</option>
                            </select>
                        </div>
                     </div>   
                      
                        
                </div>
                 <div id="tarjeta" style="display: none;">
                    <h3>Metodo de pago: Tarjeta debito.</h3>
                       <div class="form-group">
                             <input type="text" class="form-control" placeholder="Numero de comprobante" name="comprobante" />
                          </div>
                 <div class="form-group">
                     <div class="input-group">
                       <div class="input-group-addon">Banco</div> 
                             <select class="form-control" name="banco" >
                            <option value="BOD">BOD</option>
                            <option value="Provincial">Provincial</option>
                            </select>
                       </div>
                    </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Fecha" name="fecha" value="" size="32" required />
                 </div> 
                <div class="form-group">
                    <input type="text" class="form-control"  placeholder="Monto" name="monto" value="" size="32" required />
                </div>   
                    
                <button class="btn btn-success pull-right" type="submit" ><span class="glyphicon glyphicon-ok" ></span> Aceptar </button>
                 <input type="hidden" name="MM_insert" value="form1" /> 
               </form> 
       		 </div>
           </div>
         <div> 
	</div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>